<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Tests the User Field Anonymize Block rendering.
 *
 * @group user_field_anonymize
 */
class UserFieldAnonymizeBlockTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'field',
    'system',
    'user',
    'user_field_anonymize',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'block', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');

    $this->installConfig(['user_field_anonymize']);
    \Drupal::configFactory()
      ->getEditable('user_field_anonymize.settings')
      ->set('restrict_to_roles', ['only admin', 'any user', 'administrator'])
      ->save();
  }

  /**
   * Tests the rendering of block.
   */
  public function testBlockRendering(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $block_storage = $entity_type_manager->getStorage('block');

    $block = $block_storage->create([
      'id' => 'user_field_anonymize_form',
      'theme' => 'bartik',
      'plugin' => 'user_field_anonymize_block',
      'region' => 'content',
      'settings' => [
        'id' => 'user_field_anonymize_block',
        'label' => 'User Field Anonymize Form',
        'provider' => 'user_field_anonymize',
        'label_display' => '0',
      ],
    ]);
    $block->save();

    $user = $this->createUser([
      'set user profile anonymity',
    ], 'user', FALSE);
    $user->activate();
    $user->save();
    \Drupal::currentUser()->setAccount($user);

    $build = $entity_type_manager
      ->getViewBuilder($block->getEntityTypeId())
      ->view($block, 'block');

    // Assert access.
    $this->assertTrue($entity_type_manager->getAccessControlHandler('block')->createAccess($block->id(), $user));

    $render = $this->container->get('renderer')->renderRoot($build);
    $crawler = new Crawler($render->__toString());
    $actual = $crawler->filter('form#user-anonymize-form');
    $this->assertCount(1, $actual);
    $actual = $crawler->filter('input#edit-allowed-options-only-admin');
    $this->assertCount(1, $actual);
    $actual = $crawler->filter('label[for="edit-allowed-options-only-admin"]');
    $this->assertCount(1, $actual);
    $actual = $crawler->filter('input#edit-allowed-options-any-user');
    $this->assertCount(1, $actual);
    $actual = $crawler->filter('label[for="edit-allowed-options-any-user"]');
    $this->assertCount(1, $actual);
    $description = $crawler->filter('#edit-allowed-options--wrapper--description');
    $this->assertSame('Select the type of users who are allowed to access your data.', $description->text());

    // Create second user who does not have permissions.
    $user1 = $this->createUser([], 'user1', FALSE);
    $user1->activate();
    $user1->save();
    \Drupal::currentUser()->setAccount($user1);
    // Assert no access.
    $this->assertFalse($entity_type_manager->getAccessControlHandler('block')->createAccess($block->id(), $user1));
  }

}
