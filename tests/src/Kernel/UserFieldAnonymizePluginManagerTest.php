<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Kernel;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test the User Field Anonymize plugin manager.
 *
 * @group user_field_anonymize
 */
class UserFieldAnonymizePluginManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user_field_anonymize',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['user_field_anonymize']);
  }

  /**
   * Test UserFieldAnonymize plugins.
   */
  public function testUserFieldAnonymizePlugins() {
    $plugin_manager = $this->container->get('plugin.manager.user_field_anonymize');
    $definitions = $plugin_manager->getDefinitions();
    $this->assertCount(3, $plugin_manager->getDefinitions());
    $expected = new TranslatableMarkup('User Field Anonymize default plugin');
    $definition = $definitions['user_field_anonymize_default'];
    $this->assertSame((string) $expected, (string) $definition['label']);
    $this->assertSame('user_field_anonymize_default', $definition['id']);
    $this->assertSame('Drupal\user_field_anonymize\Plugin\UserFieldAnonymize\DefaultPlugin', $definition['class']);
    $this->assertSame('user_field_anonymize', $definition['provider']);

    $expected = new TranslatableMarkup('User Field Anonymize date plugin');
    $definition = $definitions['user_field_anonymize_date'];
    $this->assertSame((string) $expected, (string) $definition['label']);
    $this->assertSame('user_field_anonymize_date', $definition['id']);
    $this->assertSame('Drupal\user_field_anonymize\Plugin\UserFieldAnonymize\DatetimePlugin', $definition['class']);
    $this->assertSame('user_field_anonymize', $definition['provider']);

    $expected = new TranslatableMarkup('User Field Anonymize image plugin');
    $definition = $definitions['user_field_anonymize_image'];
    $this->assertSame((string) $expected, (string) $definition['label']);
    $this->assertSame('user_field_anonymize_image', $definition['id']);
    $this->assertSame('Drupal\user_field_anonymize\Plugin\UserFieldAnonymize\ImagePlugin', $definition['class']);
    $this->assertSame('user_field_anonymize', $definition['provider']);
  }

}
