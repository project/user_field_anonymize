<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Test the UserFieldAnonymizeSettingsForm form.
 *
 * @group user_field_anonymize
 */
class UserFieldAnonymizeSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'user_field_anonymize',
    'datetime',
    'datetime_range',
    'image',
    'file',
    'link',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    Role::create([
      'id' => 'administrator',
      'label' => 'Administrator',
    ])->save();

    Role::create([
      'id' => 'moderator',
      'label' => 'Moderator',
    ])->save();

    Role::create([
      'id' => 'editor',
      'label' => 'Editor',
    ])->save();
  }

  /**
   * Test user field anonymize configuration form.
   */
  public function testTheConfigurationForm(): void {
    $assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Assert this user does not have enough permissions.
    $this->drupalLogin($this->createUser(['access administration pages']));
    $this->drupalGet('admin/config/people/user_field_anonymize');
    $this->assertSession()->statusCodeEquals(403);

    // Give user permissions to change anonymize configuration.
    $this->drupalLogin($this->createUser(['administer user field anonymize configuration']));
    $this->drupalGet('admin/config/people/user_field_anonymize');

    // Assert that the correct roles are present.
    $assert->checkboxChecked('restrict_to_roles[only admin]');
    $assert->checkboxChecked('restrict_to_roles[any user]');
    $assert->checkboxChecked('restrict_to_roles[authenticated]');
    $assert->checkboxNotChecked('restrict_to_roles[administrator]');
    $assert->checkboxNotChecked('restrict_to_roles[moderator]');
    $assert->checkboxNotChecked('restrict_to_roles[editor]');

    // Assert that the field types are present.
    $this->assertFieldPluginMapping();

    // Select options and save.
    $options = $assert->elementExists('css', '#edit-restrict-to-roles');
    $options->checkField('restrict_to_roles[moderator]');
    $options->checkField('restrict_to_roles[administrator]');
    $page->pressButton('Save configuration');

    // Assert that administrator and moderator are selected.
    $this->drupalGet('admin/config/people/user_field_anonymize');
    $assert->checkboxChecked('restrict_to_roles[only admin]');
    $assert->checkboxChecked('restrict_to_roles[any user]');
    $assert->checkboxChecked('restrict_to_roles[authenticated]');
    $assert->checkboxChecked('restrict_to_roles[administrator]');
    $assert->checkboxChecked('restrict_to_roles[moderator]');
    $assert->checkboxNotChecked('restrict_to_roles[editor]');

    // Load the config and assert the value.
    $config = $this->config('user_field_anonymize.settings');
    $config_roles = $config->get('restrict_to_roles');
    $expected = [
      'authenticated',
      'administrator',
      'moderator',
    ];
    $this->assertSame($expected, $config_roles);

    // Change selected role.
    $options = $assert->elementExists('css', '#edit-restrict-to-roles');
    $options->checkField('restrict_to_roles[any user]');
    $options->checkField('restrict_to_roles[editor]');
    $options->uncheckField('restrict_to_roles[moderator]');
    $page->pressButton('Save configuration');

    // Assert the changes in the form.
    $this->drupalGet('admin/config/people/user_field_anonymize');
    $assert->checkboxChecked('restrict_to_roles[only admin]');
    $assert->checkboxChecked('restrict_to_roles[any user]');
    $assert->checkboxChecked('restrict_to_roles[authenticated]');
    $assert->checkboxChecked('restrict_to_roles[administrator]');
    $assert->checkboxNotChecked('restrict_to_roles[moderator]');
    $assert->checkboxChecked('restrict_to_roles[editor]');

    // Assert the config values stored.
    $this->assertFieldStoredValues();
  }

  /**
   * Asserts that the plugin values have been saved correctly.
   */
  protected function assertFieldPluginMapping(): void {
    $assert = $this->assertSession();

    // Assert that the field type option are selected by default.
    $assert->optionExists('Date', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('daterange', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('image', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('link', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('text', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('text_with_summary', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('text_long', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('email', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('integer', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('string', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('timestamp', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('entity_reference', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('decimal', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('float', 'user_field_anonymize_default')->hasAttribute('selected');
    $assert->optionExists('string_long', 'user_field_anonymize_default')->hasAttribute('selected');
  }

  /**
   * Asserts that the values stored in config are the values expected.
   */
  protected function assertFieldStoredValues(): void {
    $restrict_to_roles = [
      'authenticated',
      'administrator',
      'editor',
    ];
    $expected_field_options = [
      'datetime' => [
        'field_type' => 'datetime',
        'plugin_id' => 'user_field_anonymize_date',
      ],
      'daterange' => [
        'field_type' => 'daterange',
        'plugin_id' => 'user_field_anonymize_date',
      ],
      'image' => [
        'field_type' => 'image',
        'plugin_id' => 'user_field_anonymize_image',
      ],
      'link' => [
        'field_type' => 'link',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'text_with_summary' => [
        'field_type' => 'text_with_summary',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'text' => [
        'field_type' => 'text',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'text_long' => [
        'field_type' => 'text_long',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'integer' => [
        'field_type' => 'integer',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'entity_reference' => [
        'field_type' => 'entity_reference',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'float' => [
        'field_type' => 'float',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'email' => [
        'field_type' => 'email',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'timestamp' => [
        'field_type' => 'timestamp',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'string' => [
        'field_type' => 'string',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'string_long' => [
        'field_type' => 'string_long',
        'plugin_id' => 'user_field_anonymize_default',
      ],
      'decimal' => [
        'field_type' => 'decimal',
        'plugin_id' => 'user_field_anonymize_default',
      ],
    ];

    // Load the config and verify the value.
    $config = $this->config('user_field_anonymize.settings');
    $config_roles = $config->get('restrict_to_roles');
    $this->assertSame($restrict_to_roles, $config_roles);
    $actual_field_options = $config->get('field_options');
    // Since we loop below we need to make sure there are no other values.
    $this->assertCount(15, $actual_field_options);
    // The order of the field types is sensitive to local setup, so we loop.
    foreach ($expected_field_options as $field_type => $values) {
      $this->assertSame($values, $actual_field_options[$field_type]);
    }
    $this->assertTrue($config->get('enable_admin_users'));
    $this->assertTrue($config->get('enable_all_users'));
  }

}
