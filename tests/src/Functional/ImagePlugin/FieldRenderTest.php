<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\ImagePlugin;

/**
 * Tests default fields on user profile.
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * Tests the rendering of user fields.
   *
   * @dataProvider fieldAnonymizeScenarios
   */
  public function testRenderField(array $field_config, array $invalid_values, array $valid_values, array $empty_values, array $stored_values, array $build_values, array $user_field_anonymize, array $new_anonymous_values, array $new_anon_value): void {
    $assert = $this->assertSession();
    $field_name = $field_config['field_name'];
    $field_config['third_party_settings']['user_field_anonymize']['value']['uuid'] = $this->anonymizeImage->uuid();
    $field_config['instance_settings']['default_image']['uuid'] = $this->defaultImage->uuid();
    $field_config['storage_settings']['default_image']['uuid'] = $this->defaultImage->uuid();
    $this->createField($field_name, $field_config);
    $this->createRoles();
    $this->createUsers();

    // Check anonymized values.
    $this->drupalLogin($this->users['target']);
    $this->drupalGet("user/{$this->users['target']->id()}");
    foreach ($build_values['anonymized_value'] as $key => $value) {
      $assert->elementAttributeContains('css', 'img', $key, $value);
    }

    // Check default values.
    $this->drupalLogin($this->users['admin']);
    $this->drupalGet("user/{$this->users['target']->id()}");
    foreach ($build_values['default_value'] as $key => $value) {
      $assert->elementAttributeContains('css', 'img', $key, $value);
    }

    // Edit field and check new anonymized values.
    $this->drupalLogin($this->users['observer']);
    $user_field_anonymize['uuid'] = $this->defaultImage->uuid();
    $this->updateAnonymizeValue($field_name, $user_field_anonymize);
    $this->drupalGet("user/{$this->users['target']->id()}");
    foreach ($new_anonymous_values as $key => $value) {
      $assert->elementAttributeContains('css', 'img', $key, $value);
    }
  }

}
