<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\ImagePlugin\Views;

use Drupal\Tests\user_field_anonymize\Functional\ImagePlugin\TestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests image field in a view.
 *
 * @coversDefaultClass \Drupal\user_field_anonymize\Plugin\UserFieldAnonymize\ImagePlugin
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['user_anonymize_image_field'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    ViewTestData::createTestViews(static::class, ['user_field_anonymize_test_views']);
  }

  /**
   * Tests anonymized/not anonymized user field values in the view.
   *
   * @dataProvider fieldAnonymizeScenarios
   */
  public function testUserViewField(array $field_config, array $invalid_values, array $valid_values, array $empty_values, array $stored_values, array $build_values, array $user_field_anonymize, array $new_anonymous_values, array $users, array $user_values, array $anonymized_values): void {
    $assert = $this->assertSession();
    $field_name = $field_config['field_name'];
    $field_config['third_party_settings']['user_field_anonymize']['value']['uuid'] = $this->anonymizeImage->uuid();
    $field_config['instance_settings']['default_image']['uuid'] = $this->defaultImage->uuid();
    $field_config['storage_settings']['default_image']['uuid'] = $this->defaultImage->uuid();
    $this->createField($field_name, $field_config);
    $this->createRoles();
    $this->createUsers();
    $this->setUserValues($users, $field_name);

    // Assert default values for target and custom values.
    $this->drupalLogin($this->users['admin']);
    $this->drupalGet('user-anonymize-image-field');
    foreach ($user_values['src'] as $key => $value) {
      $assert->elementAttributeContains('css', $key, 'src', $value);
    }
    foreach ($user_values['alt'] as $key => $value) {
      $assert->elementAttributeContains('css', $key, 'alt', $value);
    }
    foreach ($user_values['title'] as $key => $value) {
      $assert->elementAttributeContains('css', $key, 'title', $value);
    }

    // Assert anonymized values.
    $this->drupalLogin($this->users['target']);
    $this->drupalGet('user-anonymize-image-field');
    foreach ($anonymized_values['src'] as $key => $value) {
      $assert->elementAttributeContains('css', $key, 'src', $value);
    }
    foreach ($anonymized_values['alt'] as $key => $value) {
      $assert->elementAttributeContains('css', $key, 'alt', $value);
    }
    foreach ($anonymized_values['title'] as $key => $value) {
      $assert->elementAttributeContains('css', $key, 'title', $value);
    }
  }

}
