<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\ImagePlugin;

/**
 * Tests image anonymized widget.
 *
 * @group user_field_anonymize
 */
class WidgetTest extends TestBase {

  /**
   * Tests widget build of the anonymized fields.
   *
   * @dataProvider FieldAnonymizeScenarios
   */
  public function testRenderField(array $field_config, array $invalid_values, array $valid_values, array $empty_values, array $stored_values): void {
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();
    $field_name = $field_config['field_name'];
    $field_config['third_party_settings']['user_field_anonymize']['value']['uuid'] = $this->anonymizeImage->uuid();
    $field_config['instance_settings']['default_image']['uuid'] = $this->defaultImage->uuid();
    $field_config['storage_settings']['default_image']['uuid'] = $this->defaultImage->uuid();
    $this->createField($field_name, $field_config);
    $this->createRoles();
    $this->createUsers();
    $this->drupalLogin($this->users['admin']);

    // Check default values.
    $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
    foreach ($valid_values as $key => $value) {
      $assert->fieldValueEquals($key, $value);
    }
    $assert->hiddenFieldValueEquals('settings[default_image][uuid][fids]', '1');
    $assert->hiddenFieldValueEquals('third_party_settings[user_field_anonymize][value][uuid][fids]', '2');

    // Empty third party values and edit the default ones.
    $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
    $page->find('css', '.form-item-third-party-settings-user-field-anonymize-value-uuid')->pressButton('Remove');
    foreach ($empty_values as $key => $value) {
      $page->fillField($key, $value);
    }
    $page->pressButton('Save settings');

    // Check the image was removed.
    $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
    $assert->hiddenFieldValueEquals('third_party_settings[user_field_anonymize][value][uuid][fids]', '');
    foreach ($empty_values as $key => $value) {
      $assert->fieldValueEquals($key, $value);
    }

    // Testing the storage.
    $field_storage = $this->fieldStorageConfigStorage->load("user.user.{$field_name}");
    $this->assertSame($stored_values, $field_storage->get('third_party_settings')['user_field_anonymize']);

    // Edit and check the values.
    $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
    foreach ($valid_values as $key => $value) {
      $page->fillField($key, $value);
    }
    $page->pressButton('Save settings');

    $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
    foreach ($valid_values as $key => $value) {
      $assert->fieldValueEquals($key, $value);
    }
  }

}
