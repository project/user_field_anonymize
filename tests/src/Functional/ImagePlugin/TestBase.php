<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\ImagePlugin;

use Drupal\file\Entity\File;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\user_field_anonymize\Functional\FieldAnonymizeTestBase;

/**
 * Field anonymize image test base.
 *
 * @group user_field_anonymize
 */
abstract class TestBase extends FieldAnonymizeTestBase {
  use TestFileCreationTrait;

  /**
   * File for the default value.
   *
   * @var \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\file\Entity\File
   */
  protected $defaultImage;

  /**
   * File for the anonymized value.
   *
   * @var \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\file\Entity\File
   */
  protected $anonymizeImage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Get and create the files.
    $files = $this->getTestFiles('image');
    $this->defaultImage = File::create((array) array_pop($files));
    $this->defaultImage->save();
    $this->anonymizeImage = File::create((array) array_pop($files));
    $this->anonymizeImage->save();
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'field_ui',
    'file',
    'image',
    'user',
    'user_field_anonymize',
    'user_field_anonymize_test_views',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $pluginTestName = 'ImagePlugin';

  /**
   * {@inheritdoc}
   */
  protected static $scenariosList = [
    'image',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUserValues(array $users, string $field_name): void {
    foreach ($users as $user_name => $fields) {
      $fields['target_id'] = $this->{$fields['image']}->id();
      unset($fields['image']);
      $this->users[$user_name]
        ->get($field_name)
        ->setValue($fields);
      $this->users[$user_name]->save();
    }
  }

}
