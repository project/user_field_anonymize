<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user_field_anonymize\Traits\FieldCreationTrait;
use Drupal\Tests\user_field_anonymize\Traits\TestDataTrait;
use Drupal\Tests\user_field_anonymize\Traits\TestUserTrait;

/**
 * Base class for field anonymization tests.
 *
 * @group user_field_anonymize
 */
abstract class FieldAnonymizeTestBase extends BrowserTestBase {

  use FieldCreationTrait;
  use TestDataTrait;
  use TestUserTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Field config storage.
   *
   * @var \Drupal\field\FieldStorageConfigStorage
   */
  protected $fieldStorageConfigStorage;

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fieldStorageConfigStorage = $this->container
      ->get('entity_type.manager')
      ->getStorage('field_config');

    $config = $this->config('user_field_anonymize.settings');
    $config->set('restrict_to_roles', [
      'administrator',
      'editor',
      'any user',
    ]);
    $config->save();
  }

}
