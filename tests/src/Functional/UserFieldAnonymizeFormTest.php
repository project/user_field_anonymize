<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Test the UserFieldAnonymizeFormTest form.
 *
 * @group user_field_anonymize
 */
class UserFieldAnonymizeFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'user_field_anonymize',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    Role::create([
      'id' => 'administrator',
      'label' => 'Administrator',
    ])->save();

    Role::create([
      'id' => 'moderator',
      'label' => 'Moderator',
    ])->save();

    Role::create([
      'id' => 'editor',
      'label' => 'Editor',
    ])->save();
  }

  /**
   * Test user "Profile Anonymity" form.
   */
  public function testTheConfigurationForm(): void {
    $assert = $this->assertSession();
    $page = $this->getSession()->getPage();
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');

    $user = $this->createUser([
      'set user profile anonymity',
    ]);
    $this->drupalLogin($user);

    // Assert authenticated user checked by default.
    $this->drupalGet('user/' . $user->id() . '/anonymize');
    $assert->checkboxNotChecked('allowed_options[only admin]');
    $assert->checkboxNotChecked('allowed_options[any user]');
    $assert->checkboxChecked('allowed_options[authenticated]');

    // Add allowed roles.
    $config = $this->config('user_field_anonymize.settings');
    $config->set('restrict_to_roles', [
      'moderator',
      'editor',
    ]);
    $config->save();

    // Assert that the correct roles are present.
    $this->drupalGet('user/' . $user->id() . '/anonymize');
    $assert->elementExists('css', 'input[value="only admin"]');
    $assert->checkboxNotChecked('allowed_options[only admin]');
    $assert->checkboxNotChecked('allowed_options[any user]');
    $assert->elementNotExists('css', 'input[value="administrator"]');
    $assert->checkboxNotChecked('allowed_options[moderator]');
    $assert->checkboxNotChecked('allowed_options[editor]');
    $assert->elementNotExists('css', 'input[value="authenticated"]');

    $config->set('restrict_to_roles', [
      'administrator',
      'moderator',
      'editor',
    ]);
    $config->save();

    // Assert that the correct roles are present.
    $this->drupalGet('user/' . $user->id() . '/anonymize');
    $assert->checkboxNotChecked('allowed_options[only admin]');
    $assert->checkboxNotChecked('allowed_options[any user]');
    $assert->checkboxNotChecked('allowed_options[administrator]');
    $assert->checkboxNotChecked('allowed_options[moderator]');
    $assert->checkboxNotChecked('allowed_options[editor]');

    // Select options and save.
    $options = $assert->elementExists('css', '#edit-allowed-options');
    $options->checkField('Moderator');
    $options->checkField('Administrator');
    $page->pressButton('Save');

    // Assert that administrator and moderator are selected.
    $this->drupalGet('user/' . $user->id() . '/anonymize');

    $assert->checkboxNotChecked('allowed_options[only admin]');
    $assert->checkboxNotChecked('allowed_options[any user]');
    $assert->checkboxChecked('allowed_options[administrator]');
    $assert->checkboxChecked('allowed_options[moderator]');
    $assert->checkboxNotChecked('allowed_options[editor]');

    $user_storage->resetCache([$user->id()]);
    $user = $user_storage->load($user->id());
    $value = $user->get('allowed_options')->first()->getValue();
    $expected = [
      'administrator' => 'administrator',
      'moderator' => 'moderator',
      'only admin' => 0,
      'any user' => 0,
      'editor' => 0,
    ];
    $this->assertSame($expected, $value);

    // Select options and save.
    $options = $assert->elementExists('css', '#edit-allowed-options');
    $options->checkField('allowed_options[only admin]');
    $options->uncheckField('allowed_options[moderator]');
    $options->checkField('allowed_options[administrator]');
    $page->pressButton('Save');

    // Check saving message.
    $assert->pageTextContains('The changes have been saved.');

    // Assert that administrator and moderator are selected.
    $this->drupalGet('user/' . $user->id() . '/anonymize');
    $assert->checkboxChecked('allowed_options[only admin]');
    $assert->checkboxNotChecked('allowed_options[any user]');
    $assert->checkboxChecked('allowed_options[administrator]');
    $assert->checkboxNotChecked('allowed_options[moderator]');
    $assert->checkboxNotChecked('allowed_options[editor]');

    $user_storage->resetCache();
    $user = $user_storage->load($user->id());
    $value = $user->get('allowed_options')->first()->getValue();
    $expected = [
      'only admin' => 'only admin',
      'administrator' => 'administrator',
      'any user' => 0,
      'moderator' => 0,
      'editor' => 0,
    ];
    $this->assertSame($expected, $value);

    // Assert I cannot see other user's anonymize settings page.
    $new_user = $this->createUser([
      'set user profile anonymity',
    ]);
    $this->drupalLogin($new_user);
    $this->drupalGet('user/' . $user->id() . '/anonymize');
    $assert->statusCodeEquals(403);

    // Assert that I cannot see any role.
    $config->set('restrict_to_roles', []);
    $config->set('enable_all_users', FALSE);
    $config->set('enable_admin_users', FALSE);
    $config->save();

    $this->drupalGet('user/' . $new_user->id() . '/anonymize');
    $this->assertSession()->statusCodeEquals(200);
    $assert->pageTextContains('No roles have been set by the site administrator.');
  }

}
