<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\EntityRefPlugin;

/**
 * Tests entity reference anonymized widget.
 *
 * @group user_field_anonymize
 */
class WidgetTest extends TestBase {

  /**
   * Tests widget build of the anonymized fields.
   */
  public function testFieldWidgetAnonymization(): void {
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();
    $data = $this->fieldsDataProvider();
    $this->createRoles();
    $this->createAllFields($data);
    $this->createUsers();

    foreach ($data as $item) {
      $field_config = $item['field_config'];
      $valid_values = $item['valid_values'];
      $invalid_values = $item['invalid_values'];
      $stored_values = $item['stored_values'];
      $field_name = $field_config['field_name'];
      $empty_values = $item['empty_values'];

      // Check default values.
      $this->drupalLogin($this->users['admin']);
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        $assert->fieldValueEquals($key, $value);
      }

      // Check invalid values.
      if (!empty($invalid_values)) {
        $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
        foreach ($invalid_values as $key => $value) {
          $page->fillField($key, $value);
        }
        $page->pressButton('Save settings');
        $assert->elementExists('xpath', '//div[@aria-label="Error message"]');
      }

      // Empty third party values and edit the default values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($empty_values as $key => $value) {
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');

      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($empty_values as $key => $value) {
        $assert->fieldValueEquals($key, $value);
      }

      // Testing the storage.
      $field_storage = $this->fieldStorageConfigStorage->load("user.user.{$field_name}");
      $this->assertSame($stored_values,
        $field_storage->get('third_party_settings')['user_field_anonymize']);

      // Edit and check the values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');

      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        $assert->fieldValueEquals($key, $value);
      }
    }
  }

}
