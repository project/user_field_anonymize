<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\EntityRefPlugin\Views;

use Drupal\Tests\user_field_anonymize\Functional\EntityRefPlugin\TestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests field using entity reference plugin in a view.
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['user_anonymize_entity_ref_field'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    ViewTestData::createTestViews(static::class, ['user_field_anonymize_test_views']);
  }

  /**
   * Tests anonymized/not anonymized user field values in the view.
   */
  public function testUserViewField(): void {
    $assert = $this->assertSession();
    $data = $this->fieldsDataProvider();
    $this->createRoles();
    $this->createAllFields($data);
    $this->createUsers();

    foreach ($data as $item) {
      $field_config = $item['field_config'];
      $field_name = $field_config['field_name'];
      $users = $item['users'];
      $user_values = $item['user_values'];
      $anonymized_values = $item['anonymized_values'];
      $this->setUserValues($users, $field_name);

      // Assert default values for target and custom values.
      $this->drupalLogin($this->users['admin']);
      $this->drupalGet('user-anonymize-entity-ref-field');
      foreach ($user_values as $key => $value) {
        $assert->elementTextContains('css', $key, $value);
      }

      // Assert anonymized values.
      $this->drupalLogin($this->users['target']);
      $this->drupalGet('user-anonymize-entity-ref-field');
      foreach ($anonymized_values as $key => $value) {
        $assert->elementTextContains('css', $key, $value);
      }
    }

  }

}
