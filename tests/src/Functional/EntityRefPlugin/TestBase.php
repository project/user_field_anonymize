<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\EntityRefPlugin;

use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\Tests\user_field_anonymize\Functional\FieldAnonymizeTestBase;

/**
 * Field anonymize entity reference test base.
 *
 * @group user_field_anonymize
 */
abstract class TestBase extends FieldAnonymizeTestBase {
  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // In drupal 9.3 'entity_reference' is obsolete.
    if (\Drupal::service('module_handler')->moduleExists('entity_reference')) {
      \Drupal::service("module_installer")->install(['entity_reference']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'field_ui',
    'taxonomy',
    'user',
    'user_field_anonymize',
    'user_field_anonymize_test_views',
  ];

  /**
   * Term one.
   *
   * @var array
   */
  protected $term;

  /**
   * Term two.
   *
   * @var array
   */
  protected $termTwo;

  /**
   * Term three.
   *
   * @var array
   */
  protected $termThree;

  /**
   * Contains saving, validation, storage and build data provider.
   *
   * @return array
   *   The test data.
   */
  public function fieldsDataProvider(): array {
    $data = [];

    // Covers EntityReferencePlugin.
    $vocabulary = $this->createVocabulary();
    $this->term = $this->createTerm($vocabulary, [
      'name' => 'Term one',
    ]);
    $this->termTwo = $this->createTerm($vocabulary, [
      'name' => 'Term two',
    ]);
    $this->termThree = $this->createTerm($vocabulary, [
      'name' => 'Term three',
    ]);

    $data['field_taxonomy_term'] = [
      'field_config' => [
        'field_name' => 'field_taxonomy_term',
        'field_type' => 'entity_reference',
        'field_view_type' => 'entity_reference_label',
        'field_form_type' => 'entity_reference_autocomplete',
        'storage_settings' => [
          'target_type' => 'taxonomy_term',
        ],
        'instance_settings' => [
          'handler' => 'default:taxonomy_term',
          'handler_settings' => [
            'target_bundles' => [
              $vocabulary->id() => $vocabulary->id(),
            ],
          ],
        ],
        'cardinality' => 1,
        'form_entity_display' => 'user',
        'third_party_settings' => [
          'user_field_anonymize' => [
            'enabled' => TRUE,
            'value' => [
              ['target_uuid' => $this->term->uuid()],
            ],
          ],
        ],
        'default_value_input' => [
          'value' => ['target_uuid' => $this->termTwo->uuid()],
        ],
      ],
      'invalid_values' => [
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term][0][target_id]' => 'wrong value',
      ],
      'valid_values' => [
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term][0][target_id]' => 'Term one (1)',
        'default_value_input[field_taxonomy_term][0][target_id]' => 'Term two (2)',
      ],
      'empty_values' => [
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term][0][target_id]' => '',
        'default_value_input[field_taxonomy_term][0][target_id]' => 'Term three (3)',
      ],
      'stored_values' => [
        'enabled' => TRUE,
        'value' => [],
      ],
      'build_values' => [
        'anonymized_value' => [
          'value' => 'Term one',
        ],
        'default_value' => [
          'value' => 'Term two',
        ],
      ],
      'user_field_anonymize' => [
        ['target_uuid' => $this->termTwo->uuid()],
      ],
      'new_anonymous_values' => [
        'Term three' => 'Term two',
      ],
      'users' => [
        'admin' => [
          'target_id' => $this->termThree->id(),
        ],
        'target' => [
          'target_id' => $this->term->id(),
        ],
        'observer' => [
          'target_id' => $this->termTwo->id(),
        ],
      ],
      'user_values' => [
        '.views-row:nth-child(1) div.views-field-name a' => 'admin',
        '.views-row:nth-child(2) div.views-field-name a' => 'target',
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term .field-content' => 'Term one',
        '.views-row:nth-child(3) div.views-field-name a' => 'new_admin',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term .field-content' => 'Term three',
        '.views-row:nth-child(4) div.views-field-name a' => 'observer',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term .field-content' => 'Term two',
      ],
      'anonymized_values' => [
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term .field-content' => 'Term one',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term .field-content' => 'Term one',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term .field-content' => 'Term one',
      ],
    ];
    $data['field_taxonomy_term_multiple'] = [
      'field_config' => [
        'field_name' => 'field_taxonomy_term_multi',
        'field_type' => 'entity_reference',
        'field_view_type' => 'entity_reference_label',
        'field_form_type' => 'entity_reference_autocomplete',
        'storage_settings' => [
          'target_type' => 'taxonomy_term',
        ],
        'instance_settings' => [
          'handler' => 'default:taxonomy_term',
          'handler_settings' => [
            'target_bundles' => [
              $vocabulary->id() => $vocabulary->id(),
            ],
          ],
        ],
        'cardinality' => 3,
        'form_entity_display' => 'user',
        'third_party_settings' => [
          'user_field_anonymize' => [
            'enabled' => TRUE,
            'value' => [
                ['target_uuid' => $this->term->uuid()],
                ['target_uuid' => $this->termTwo->uuid()],
                ['target_uuid' => $this->termThree->uuid()],
            ],
          ],
        ],
        'default_value_input' => [
          ['target_uuid' => $this->termTwo->uuid()],
          ['target_uuid' => $this->term->uuid()],
          ['target_uuid' => $this->termThree->uuid()],
        ],
      ],
      'invalid_values' => [
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][0][target_id]' => 'wrong value',
      ],
      'valid_values' => [
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][0][target_id]' => 'Term one (1)',
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][1][target_id]' => 'Term two (2)',
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][2][target_id]' => 'Term three (3)',
        'default_value_input[field_taxonomy_term_multi][0][target_id]' => 'Term two (2)',
        'default_value_input[field_taxonomy_term_multi][1][target_id]' => 'Term one (1)',
        'default_value_input[field_taxonomy_term_multi][2][target_id]' => 'Term three (3)',
      ],
      'empty_values' => [
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][0][target_id]' => '',
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][1][target_id]' => '',
        'third_party_settings[user_field_anonymize][value][field_taxonomy_term_multi][2][target_id]' => '',
        'default_value_input[field_taxonomy_term_multi][0][target_id]' => 'Term one (1)',
        'default_value_input[field_taxonomy_term_multi][1][target_id]' => 'Term one (1)',
        'default_value_input[field_taxonomy_term_multi][2][target_id]' => 'Term two (2)',
      ],
      'stored_values' => [
        'enabled' => TRUE,
        'value' => [],
      ],
      'build_values' => [
        'anonymized_value' => [
          'value1' => 'Term one',
          'value2' => 'Term two',
          'value3' => 'Term three',
        ],
        'default_value' => [
          'value1' => 'Term two',
          'value2' => 'Term one',
          'value3' => 'Term three',
        ],
      ],
      'user_field_anonymize' => [
          ['target_uuid' => $this->termTwo->uuid()],
          ['target_uuid' => $this->term->uuid()],
          ['target_uuid' => $this->termThree->uuid()],
      ],
      'new_anonymous_values' => [
        'Term one' => 'Term two',
        'Term two' => 'Term one',
        'Term three' => 'Term three',
      ],
      'users' => [
        'admin' => [
          ['target_id' => intval($this->term->id())],
          ['target_id' => intval($this->termTwo->id())],
          ['target_id' => intval($this->termThree->id())],
        ],
        'target' => [
          ['target_id' => intval($this->termThree->id())],
          ['target_id' => intval($this->termTwo->id())],
          ['target_id' => intval($this->term->id())],
        ],
        'observer' => [
          ['target_id' => intval($this->termThree->id())],
          ['target_id' => intval($this->term->id())],
          ['target_id' => intval($this->termTwo->id())],
        ],
      ],
      'user_values' => [
        '.views-row:nth-child(1) div.views-field-name a' => 'admin',
        '.views-row:nth-child(2) div.views-field-name a' => 'target',
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(1)' => 'Term three',
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(2)' => 'Term two',
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(3)' => 'Term one',
        '.views-row:nth-child(3) div.views-field-name a' => 'new_admin',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(1)' => 'Term one',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(2)' => 'Term two',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(3)' => 'Term three',
        '.views-row:nth-child(4) div.views-field-name a' => 'observer',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(1)' => 'Term three',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(2)' => 'Term one',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(3)' => 'Term two',
      ],
      'anonymized_values' => [
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(1)' => 'Term one',
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(2)' => 'Term two',
        '.views-row:nth-child(2) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(3)' => 'Term three',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(1)' => 'Term one',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(2)' => 'Term two',
        '.views-row:nth-child(3) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(3)' => 'Term three',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(1)' => 'Term one',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(2)' => 'Term two',
        '.views-row:nth-child(4) div.views-field-field-taxonomy-term-multi .field-content a:nth-child(3)' => 'Term three',
      ],
    ];

    $data['field_users'] = [
      'field_config' => [
        'field_name' => 'field_users',
        'field_type' => 'entity_reference',
        'field_view_type' => 'entity_reference_entity_id',
        'field_form_type' => 'entity_reference_autocomplete',
        'storage_settings' => [
          'target_type' => 'user',
        ],
        'instance_settings' => [
          'handler' => 'default:user',
          'handler_settings' => [
            'include_anonymous' => TRUE,
          ],
        ],
        'cardinality' => 1,
        'form_entity_display' => 'user',
        'third_party_settings' => [
          'user_field_anonymize' => [
            'enabled' => TRUE,
            'value' => ['target_id' => 0],
          ],
        ],
        'default_value_input' => [
          'value' => ['target_id' => 1],
        ],
      ],
      'invalid_values' => [
        'default_value_input[field_users][0][target_id]' => 'wrong value',
      ],
      'valid_values' => [
        'third_party_settings[user_field_anonymize][value][field_users][0][target_id]' => 'Anonymous (0)',
        'default_value_input[field_users][0][target_id]' => 'admin (1)',
      ],
      'empty_values' => [
        'third_party_settings[user_field_anonymize][value][field_users][0][target_id]' => '',
        'default_value_input[field_users][0][target_id]' => 'target (2)',
      ],
      'stored_values' => [
        'enabled' => TRUE,
        'value' => [],
      ],
      'build_values' => [
        'anonymized_value' => [],
        'default_value' => [
          'value' => 1,
        ],
      ],
      'user_field_anonymize' => [
        'target_id' => 3,
      ],
      'new_anonymous_values' => [
        'admin' => 3,
      ],
      'users' => [
        'admin' => [
          'target_id' => 0,
        ],
        'target' => [
          'target_id' => 1,
        ],
        'observer' => [
          'target_id' => 1,
        ],
      ],
      'user_values' => [
        '.views-row:nth-child(1) div.views-field-name a' => 'admin',
        '.views-row:nth-child(2) div.views-field-name a' => 'target',
        '.views-row:nth-child(2) div.views-field-field-users .field-content' => 'admin',
        '.views-row:nth-child(3) div.views-field-name a' => 'new_admin',
        '.views-row:nth-child(3) div.views-field-field-users .field-content' => 'Anonymous',
        '.views-row:nth-child(4) div.views-field-name a' => 'observer',
        '.views-row:nth-child(4) div.views-field-field-users .field-content' => 'admin',
      ],
      'anonymized_values' => [
        '.views-row:nth-child(2) div.views-field-field-users .field-content' => 'Anonymous',
        '.views-row:nth-child(3) div.views-field-field-users .field-content' => 'Anonymous',
        '.views-row:nth-child(4) div.views-field-field-users .field-content' => 'Anonymous',
      ],
    ];
    $data['field_users_multiple'] = [
      'field_config' => [
        'field_name' => 'field_users_multiple',
        'field_type' => 'entity_reference',
        'field_view_type' => 'entity_reference_entity_id',
        'field_form_type' => 'entity_reference_autocomplete',
        'storage_settings' => [
          'target_type' => 'user',
        ],
        'instance_settings' => [
          'handler' => 'default:user',
          'handler_settings' => [
            'include_anonymous' => TRUE,
          ],
        ],
        'cardinality' => 3,
        'form_entity_display' => 'user',
        'third_party_settings' => [
          'user_field_anonymize' => [
            'enabled' => TRUE,
            'value' => [
              ['target_id' => 1],
              ['target_id' => 0],
              ['target_id' => 1],
            ],
          ],
        ],
        'default_value_input' => [
          ['target_id' => 1],
        ],
      ],
      'invalid_values' => [
        'default_value_input[field_users_multiple][0][target_id]' => 'wrong value',
      ],
      'valid_values' => [
        'third_party_settings[user_field_anonymize][value][field_users_multiple][0][target_id]' => 'admin (1)',
        'third_party_settings[user_field_anonymize][value][field_users_multiple][1][target_id]' => 'Anonymous (0)',
        'third_party_settings[user_field_anonymize][value][field_users_multiple][2][target_id]' => 'admin (1)',
        'default_value_input[field_users_multiple][0][target_id]' => 'admin (1)',
      ],
      'empty_values' => [
        'third_party_settings[user_field_anonymize][value][field_users_multiple][0][target_id]' => '',
        'third_party_settings[user_field_anonymize][value][field_users_multiple][1][target_id]' => '',
        'third_party_settings[user_field_anonymize][value][field_users_multiple][2][target_id]' => '',
        'default_value_input[field_users_multiple][0][target_id]' => 'new_admin (3)',
        'default_value_input[field_users_multiple][1][target_id]' => 'observer (4)',
      ],
      'stored_values' => [
        'enabled' => TRUE,
        'value' => [],
      ],
      'build_values' => [
        'anonymized_value' => [
          'value1' => 1,
          'value2' => 1,
        ],
        'default_value' => [
          'value1' => 1,
        ],
      ],
      'user_field_anonymize' => [
        ['target_id' => '0'],
        ['target_id' => '0'],
        ['target_id' => '1'],
      ],
      'new_anonymous_values' => [
        'value' => 1,
      ],
      'users' => [
        'admin' => [
          ['target_id' => 4],
          ['target_id' => 2],
          ['target_id' => 3],
        ],
        'target' => [
          ['target_id' => 1],
          ['target_id' => 2],
          ['target_id' => 4],
        ],
        'observer' => [
          ['target_id' => 4],
          ['target_id' => 2],
          ['target_id' => 2],
        ],
      ],
      'user_values' => [
        '.views-row:nth-child(1) div.views-field-name a' => 'admin',
        '.views-row:nth-child(2) div.views-field-name a' => 'target',
        '.views-row:nth-child(2) div.views-field-field-users-multiple .field-content ul li:nth-child(1)' => 'admin',
        '.views-row:nth-child(2) div.views-field-field-users-multiple .field-content ul li:nth-child(2)' => 'target',
        '.views-row:nth-child(2) div.views-field-field-users-multiple .field-content ul li:nth-child(3)' => 'observer',
        '.views-row:nth-child(3) div.views-field-name a' => 'new_admin',
        '.views-row:nth-child(3) div.views-field-field-users-multiple .field-content ul li:nth-child(1)' => 'observer',
        '.views-row:nth-child(3) div.views-field-field-users-multiple .field-content ul li:nth-child(2)' => 'target',
        '.views-row:nth-child(3) div.views-field-field-users-multiple .field-content ul li:nth-child(3)' => 'admin',
        '.views-row:nth-child(4) div.views-field-name a' => 'observer',
        '.views-row:nth-child(4) div.views-field-field-users-multiple .field-content ul li:nth-child(1)' => 'observer',
        '.views-row:nth-child(4) div.views-field-field-users-multiple .field-content ul li:nth-child(2)' => 'target',
        '.views-row:nth-child(4) div.views-field-field-users-multiple .field-content ul li:nth-child(3)' => 'target',
      ],
      'anonymized_values' => [
        '.views-row:nth-child(2) div.views-field-field-users-multiple .field-content ul li:nth-child(1)' => 'admin',
        '.views-row:nth-child(2) div.views-field-field-users-multiple .field-content ul li:nth-child(2)' => 'Anonymous',
        '.views-row:nth-child(2) div.views-field-field-users-multiple .field-content ul li:nth-child(3)' => 'admin',
        '.views-row:nth-child(3) div.views-field-field-users-multiple .field-content ul li:nth-child(1)' => 'admin',
        '.views-row:nth-child(3) div.views-field-field-users-multiple .field-content ul li:nth-child(2)' => 'Anonymous',
        '.views-row:nth-child(3) div.views-field-field-users-multiple .field-content ul li:nth-child(3)' => 'admin',
        '.views-row:nth-child(4) div.views-field-field-users-multiple .field-content ul li:nth-child(1)' => 'admin',
        '.views-row:nth-child(4) div.views-field-field-users-multiple .field-content ul li:nth-child(2)' => 'Anonymous',
        '.views-row:nth-child(4) div.views-field-field-users-multiple .field-content ul li:nth-child(3)' => 'admin',
      ],
    ];

    return $data;
  }

  /**
   * Creates the fields for the entity ref.
   *
   * It is needed in this plugin to add default values
   * when creating users.
   *
   * @param array $data
   *   Contains all fields.
   */
  protected function createAllFields(array $data): void {
    foreach ($data as $item) {
      $field_config = $item['field_config'];
      $field_name = $field_config['field_name'];
      $this->createField($field_name, $field_config);
    }
  }

}
