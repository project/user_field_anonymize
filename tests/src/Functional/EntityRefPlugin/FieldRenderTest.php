<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\EntityRefPlugin;

/**
 * Tests entity reference anonymized plugin.
 *
 * @coversDefaultClass \Drupal\user_field_anonymize\Plugin\UserFieldAnonymize\DefaultPlugin
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * Tests the saving and validation of the anonymized field.
   */
  public function testRenderField(): void {
    $assert = $this->assertSession();
    $data = $this->fieldsDataProvider();
    $this->createRoles();
    $this->createAllFields($data);
    $this->createUsers();

    foreach ($data as $item) {
      $field_config = $item['field_config'];
      $build_values = $item['build_values'];
      $user_field_anonymize = $item['user_field_anonymize'];
      $new_anonymous_values = $item['new_anonymous_values'];
      $field_name = $field_config['field_name'];

      // Check anonymized values.
      $this->drupalLogin($this->users['observer']);
      $this->drupalGet("user/{$this->users['target']->id()}");
      foreach ($build_values['anonymized_value'] as $value) {
        $assert->elementTextContains('css', 'article', (string) $value);
      }

      // Check default values.
      $this->drupalLogin($this->users['admin']);
      $this->drupalGet("user/{$this->users['target']->id()}");
      foreach ($build_values['default_value'] as $value) {
        $assert->elementTextContains('css', 'article', (string) $value);
      }

      // Edit field and check new anonymized values.
      $this->drupalLogin($this->users['observer']);
      $this->updateAnonymizeValue($field_name, $user_field_anonymize);
      $this->drupalGet("user/{$this->users['target']->id()}");
      foreach ($new_anonymous_values as $new_anon_value) {
        $assert->elementTextContains('css', 'article', (string) $new_anon_value);
      }
    }
  }

}
