<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\DefaultPlugin;

/**
 * Tests widget build of the anonymized fields.
 *
 * @group user_field_anonymize
 */
class WidgetTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createRoles();
    $this->createUsers();
  }

  /**
   * Tests widget build of the anonymized fields.
   */
  public function testFieldWidget(): void {
    $scenarios = $this->fieldAnonymizeScenarios();
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    foreach ($scenarios as $scenario) {
      $field_config = $scenario['field_config'];
      $valid_values = $scenario['valid_values'];
      $invalid_values = $scenario['invalid_values'];
      $empty_values = $scenario['empty_values'];
      $stored_values = $scenario['stored_values'];
      $field_name = $field_config['field_name'];
      $field_type = $field_config['field_view_type'];
      $multiple = $field_config['multiple'] ?? FALSE;
      $this->createField($field_name, $field_config);
      $this->drupalLogin($this->users['admin']);

      // Check default values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        if ($field_type == 'list_default') {
          if ($multiple) {
            foreach ($value as $multiple_value) {
              $assert->optionExists($key, $multiple_value)
                ->hasAttribute('selected');
            }
            continue;
          }
          $assert->optionExists($key, $value)->hasAttribute('selected');
          continue;
        }
        $assert->fieldValueEquals($key, $value);
      }

      // Check invalid values.
      if (!empty($invalid_values)) {
        $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
        foreach ($invalid_values as $key => $value) {
          if ($field_type == 'list_default') {
            $page->selectFieldOption($key, $value);
            continue;
          }
          $page->fillField($key, $value);
        }
        $page->pressButton('Save settings');
        $assert->elementExists('xpath', '//div[@aria-label="Error message"]');
      }

      // Empty third party values and edit the default values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($empty_values as $key => $value) {
        if ($field_type == 'list_default') {
          $page->selectFieldOption($key, $value);
          continue;
        }
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');

      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($empty_values as $key => $value) {
        if ($field_type == 'list_default') {
          $assert->optionExists($key, $value)->hasAttribute('selected');
          continue;
        }
        $assert->fieldValueEquals($key, $value);
      }

      // Testing the storage.
      $field_storage = $this->fieldStorageConfigStorage->load("user.user.{$field_name}");
      $this->assertSame($stored_values, $field_storage->get('third_party_settings')['user_field_anonymize']);

      // Edit and check the values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        if ($field_type == 'list_default') {
          if ($multiple) {
            foreach ($value as $multiple_value) {
              $page->selectFieldOption($key, $multiple_value);
            }
            continue;
          }
          $page->selectFieldOption($key, $value);
          continue;
        }
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');

      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        if ($field_type == 'list_default') {
          if ($multiple) {
            foreach ($value as $multiple_value) {
              $assert->optionExists($key, $multiple_value)
                ->hasAttribute('selected');
            }
            continue;
          }
          $assert->optionExists($key, $value)->hasAttribute('selected');
          continue;
        }
        $assert->fieldValueEquals($key, $value);
      }

      // We don't run this in isolation, so we have to clean up.
      $this->removeField($field_name);
    }
  }

}
