<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\DefaultPlugin;

use Drupal\Tests\user_field_anonymize\Functional\FieldAnonymizeTestBase;

/**
 * Field anonymize default test base.
 *
 * @group user_field_anonymize
 */
abstract class TestBase extends FieldAnonymizeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $pluginTestName = 'DefaultPlugin';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'field_ui',
    'link',
    'options',
    'user',
    'user_field_anonymize',
    'user_field_anonymize_test_views',
  ];

  /**
   * List of scenarios to be tested.
   *
   * @var string[]
   */
  protected static $scenariosList = [
    'email',
    'float_list',
    'integer_list',
    'string_list',
    'link',
    'number_decimal',
    'number_float',
    'number_integer',
    'text_formatted',
    'text_plain',
    'text_trimmed',
  ];

}
