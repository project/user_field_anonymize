<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\DefaultPlugin\Views;

use Drupal\Tests\user_field_anonymize\Functional\DefaultPlugin\TestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests field using default plugin in a view.
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['user_anonymize_default_field'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createRoles();
    $this->createUsers();

    ViewTestData::createTestViews(static::class, ['user_field_anonymize_test_views']);
  }

  /**
   * Tests anonymized/not anonymized user field values in the view.
   *
   * @dataProvider fieldAnonymizeScenarios
   */
  public function testUserViewField(array $field_config, array $invalid_values, array $valid_values, array $empty_values, array $stored_values, array $build_values, array $user_field_anonymize, array $new_anonymous_values, array $users, array $user_values, array $anonymized_values): void {
    $assert = $this->assertSession();

    $field_name = $field_config['field_name'];
    $this->createField($field_name, $field_config);
    $this->setUserValues($users, $field_name);

    // Assert default values for target and custom values.
    $this->drupalLogin($this->users['admin']);
    $this->drupalGet('user-anonymize-default-field');
    foreach ($user_values as $key => $value) {
      $assert->elementTextContains('css', $key, $value);
    }

    // Assert anonymized values.
    $this->drupalLogin($this->users['target']);
    $this->drupalGet('user-anonymize-default-field');
    foreach ($anonymized_values as $key => $value) {
      $assert->elementTextContains('css', $key, $value);
    }
  }

}
