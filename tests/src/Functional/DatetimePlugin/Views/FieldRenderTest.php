<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\DatetimePlugin\Views;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\user_field_anonymize\Functional\DatetimePlugin\TestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests field using datetime in a view.
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['user_anonymize_datetime_field'];

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    ViewTestData::createTestViews(static::class, ['user_field_anonymize_test_views']);

    $this->dateFormatter = $this->container->get('date.formatter');

    // Set the default timezone to Brussels.
    $this->config('system.date')
      ->set('timezone.default', 'UTC')
      ->save();
  }

  /**
   * Tests anonymized/not anonymized user field values in the view.
   *
   * @dataProvider fieldAnonymizeScenarios
   */
  public function testUserViewField(array $field_config, array $invalid_values, array $valid_values, array $empty_values, array $stored_values, array $build_values, array $user_field_anonymize, array $new_anonymous_values, array $users, array $user_values, array $anonymized_values): void {
    $assert = $this->assertSession();
    $field_name = $field_config['field_name'];
    $this->createField($field_name, $field_config);
    $this->createRoles();
    $this->createUsers();
    $this->setUserValues($users, $field_name);

    // Assert default values for target and custom values.
    $this->drupalLogin($this->users['admin']);
    $this->drupalGet('user-anonymize-datetime-field');
    foreach ($user_values as $key => $value) {
      $assert->elementTextContains('css', $key,
        $this->dateFormatter->format(strtotime((string) $value), timezone : DateTimeItemInterface::STORAGE_TIMEZONE));
    }

    // Assert anonymized values.
    $this->drupalLogin($this->users['target']);
    $this->drupalGet('user-anonymize-datetime-field');
    foreach ($anonymized_values as $key => $value) {
      $assert->elementTextContains('css', $key,
        $this->dateFormatter->format(strtotime((string) $value), timezone : DateTimeItemInterface::STORAGE_TIMEZONE));
    }
  }

}
