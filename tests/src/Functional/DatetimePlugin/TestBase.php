<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\DatetimePlugin;

use Drupal\Tests\user_field_anonymize\Functional\FieldAnonymizeTestBase;

/**
 * Field anonymize datetime test base.
 *
 * @group user_field_anonymize
 */
abstract class TestBase extends FieldAnonymizeTestBase {

  /**
   * Plugin name folder to fetch the fixtures.
   *
   * @var string
   */
  protected static $pluginTestName = 'DatetimePlugin';

  /**
   * List of scenarios to be tested.
   *
   * @var string[]
   */
  protected static $scenariosList = [
    'date_range',
    'date_time',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'datetime_range',
    'field',
    'field_ui',
    'user',
    'user_field_anonymize',
    'user_field_anonymize_test_views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUserValues(array $users, string $field_name): void {
    foreach ($users as $user_name => $fields) {
      foreach ($fields as $key => $value) {
        $val[$key] = date('Y-m-d\TH:i:s', strtotime((string) $value));
      }
      $this->users[$user_name]
        ->get($field_name)
        ->setValue([$val]);
      $this->users[$user_name]->save();
    }
  }

}
