<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user_field_anonymize\Traits\FieldCreationTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Test the field's visibility and displayed values depending on user settings.
 *
 * @group user_field_anonymize
 */
class FieldsVisibilityTest extends BrowserTestBase {

  use FieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'user_field_anonymize',
    'options',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Field with anonymized value.
    $this->createField('field_anonymized', [
      'field_type' => 'string',
      'storage_settings' => [],
      'cardinality' => 1,
      'instance_settings' => [],
      'third_party_settings' => [
        'user_field_anonymize' => [
          'enabled' => TRUE,
          'value' => [
            [
              'value' => 'Anonymized field value',
            ],
          ],
        ],
      ],
      'default_value_input' => [
        [
          'value' => 'Value 1',
        ],
      ],
      'field_view_type' => 'string',
      'field_form_type' => 'text_textfield',
      'form_entity_display' => 'user',
    ]);
    // Field without anonymized value.
    $this->createField('field_not_anonymized', [
      'field_type' => 'string',
      'storage_settings' => [],
      'cardinality' => 1,
      'instance_settings' => [],
      'default_value_input' => [
        [
          'value' => 'Value 2',
        ],
      ],
      'field_view_type' => 'string',
      'field_form_type' => 'text_textfield',
      'form_entity_display' => 'user',
    ]);

    Role::create([
      'id' => 'editor',
      'label' => 'editor',
    ])->save();

    // Give anonymous users permission to view user profiles, so that we can
    // verify the default behaviour.
    $user_role = Role::load(RoleInterface::ANONYMOUS_ID);
    $user_role->grantPermission('access user profiles');
    $user_role->save();
  }

  /**
   * Tests the visibility of an anonymized field.
   */
  public function testFieldsVisibility(): void {
    $observer = $this->createUser(['access user profiles']);
    $observer->addRole('editor');
    $observer->save();
    $target = $this->createUser(['access user profiles']);

    // Assert anonymous cannot see fields, only anonymized.
    $this->drupalGet('/user/' . $target->id());
    $this->assertUserSeesAnonymizeValues();

    // Log in and assert user can see field values.
    $this->drupalLogin($observer);
    $this->drupalGet('/user/' . $target->id());
    $this->assertUserCanSeeFieldValues();

    // Change target's anonymize selection to empty,
    // assert authenticated can only see anonymized now.
    $target->get('allowed_options')->setValue([
      'administrator' => 0,
      'editor' => 0,
      'authenticated' => 0,
    ]);
    $target->save();
    $this->drupalGet('/user/' . $target->id());
    $this->assertUserSeesAnonymizeValues();

    // Change target's anonymize selection to editor,
    // assert authenticated can only see field values.
    $target->get('allowed_options')->setValue(['editor' => 'editor']);
    $target->save();
    $this->drupalGet('/user/' . $target->id());
    $this->assertUserCanSeeFieldValues();

    // Change target's anonymize selection to a different role,
    // assert authenticated can only see field values.
    // Change target's anonymize selection to empty,
    // assert authenticated can only see anonymize now.
    $target->get('allowed_options')->setValue(['someone' => 'someone']);
    $target->save();
    $this->drupalGet('/user/' . $target->id());
    $this->assertUserSeesAnonymizeValues();
  }

  /**
   * Assert the logged-in user can see the specific field values.
   */
  protected function assertUserCanSeeFieldValues(): void {
    $assert = $this->assertSession();
    $assert->elementTextContains('css', 'article', 'Value 1');
    $assert->elementTextContains('css', 'article', 'Value 2');
  }

  /**
   * Assert the logged-in user can see only anonymize values.
   */
  protected function assertUserSeesAnonymizeValues(): void {
    $assert = $this->assertSession();
    $assert->elementTextContains('css', 'article', 'Anonymized field value');
    $assert->elementTextNotContains('css', 'article', 'Value 1');
    $assert->elementTextNotContains('css', 'article', 'Value 2.');
  }

}
