<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\TimestampPlugin;

/**
 * Tests timestamp widget.
 *
 * @group user_field_anonymize
 */
class WidgetTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createRoles();
    $this->createUsers();
  }

  /**
   * Tests widget build of the anonymized fields.
   *
   * @dataProvider fieldAnonymizeScenarios
   */
  public function testFieldWidget(): void {
    $scenarios = $this->fieldAnonymizeScenarios();
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    foreach ($scenarios as $scenario) {
      $field_config = $scenario['field_config'];
      $valid_values = $scenario['valid_values'];
      $empty_values = $scenario['empty_values'];
      $invalid_values = $scenario['invalid_values'];
      $field_name = $field_config['field_name'];
      $this->createField($field_name, $field_config);

      // Check default values.
      $this->drupalLogin($this->users['admin']);
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        $assert->fieldValueEquals($key, $value);
      }

      // Assert invalid values throw error.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($invalid_values as $key => $value) {
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');
      $assert->elementExists('css', '[aria-label="Error message"]');

      // Empty third party values and edit the default values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($empty_values['values_to_save'] as $key => $value) {
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');

      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($empty_values['values_to_check'] as $key => $value) {
        $assert->fieldValueEquals($key, date("Y-m-d", strtotime((string) $value)));
      }

      // Testing the storage.
      $field_storage = $this->fieldStorageConfigStorage->load("user.user.{$field_name}");
      $storage = $field_storage->get('third_party_settings')['user_field_anonymize']['value'];
      $current_time = \Drupal::time()->getCurrentTime();
      $now = date('Y-m-d', $current_time);
      $stored_date = date('Y-m-d', $storage[0]['value']);
      $this->assertSame($now, $stored_date);

      // Edit and check the values.
      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        $page->fillField($key, $value);
      }
      $page->pressButton('Save settings');

      $this->drupalGet("admin/config/people/accounts/fields/user.user.{$field_name}");
      foreach ($valid_values as $key => $value) {
        $assert->fieldValueEquals($key, $value);
      }

      // We don't run this in isolation, so we have to clean up.
      $this->removeField($field_name);
    }
  }

}
