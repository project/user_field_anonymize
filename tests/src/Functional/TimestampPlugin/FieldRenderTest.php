<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\TimestampPlugin;

use Drupal\Core\Datetime\Entity\DateFormat;

/**
 * Tests timestamp fields on user profile.
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createRoles();
    $this->createUsers();

    // Make sure medium date format has 'D, j M Y - H:i' pattern.
    DateFormat::load('medium')
      ->setPattern('D, j M Y - H:i')
      ->save();
  }

  /**
   * Tests the saving and validation of the anonymized field.
   */
  public function testRenderField(): void {
    $scenarios = $this->fieldAnonymizeScenarios();
    $assert = $this->assertSession();

    foreach ($scenarios as $scenario) {
      $field_config = $scenario['field_config'];
      $build_values = $scenario['build_values'];
      $user_field_anonymize = $scenario['user_field_anonymize'];
      $new_anonymous_values = $scenario['new_anonymous_values'];
      $field_name = $field_config['field_name'];
      $this->createField($field_name, $field_config);

      // Check anonymized values.
      $this->drupalLogin($this->users['observer']);
      $this->drupalGet("user/{$this->users['target']->id()}");
      foreach ($build_values['anonymized_value'] as $value) {
        $assert->elementTextContains('css', 'article', $value);
      }

      // Check default values.
      // We create a new user so default values get picked up.
      $user = $this->createUser();
      $user->get('allowed_options')->setValue(['editor' => 'editor']);
      $user->addRole('editor');
      $user->save();

      $this->drupalLogin($user);
      $this->drupalGet("user/{$user->id()}");
      foreach ($build_values['default_value'] as $value) {
        $assert->elementTextContains('css', 'article', $value);
      }

      // Edit field and check new anonymized values.
      $this->drupalLogin($this->users['observer']);
      $this->updateAnonymizeValue($field_name, $user_field_anonymize);
      $this->drupalGet("user/{$this->users['target']->id()}");
      foreach ($new_anonymous_values as $new_anon_value) {
        $assert->elementTextContains('css', 'article', $new_anon_value);
      }

      // We don't run this in isolation, so we have to clean up.
      $this->removeField($field_name);
    }
  }

}
