<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\TimestampPlugin\Views;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Tests\user_field_anonymize\Functional\TimestampPlugin\TestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests field using timestamp in a view.
 *
 * @coversDefaultClass \Drupal\user_field_anonymize\Plugin\UserFieldAnonymize\DatetimePlugin
 *
 * @group user_field_anonymize
 */
class FieldRenderTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['user_anonymize_timestamp_field'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Make sure medium date format has 'D, j M Y - H:i' pattern.
    DateFormat::load('medium')
      ->setPattern('D, j M Y - H:i')
      ->save();

    ViewTestData::createTestViews(static::class, ['user_field_anonymize_test_views']);
  }

  /**
   * Tests anonymized/not anonymized user field values in the view.
   *
   * @dataProvider fieldAnonymizeScenarios
   */
  public function testUserViewField(array $field_config, array $invalid_values, array $valid_values, array $empty_values, array $stored_values, array $build_values, array $user_field_anonymize, array $new_anonymous_values, array $users, array $user_values, array $anonymized_values): void {
    $assert = $this->assertSession();
    $field_name = $field_config['field_name'];
    $this->createField($field_name, $field_config);
    $this->createRoles();
    $this->createUsers();
    $this->setUserValues($users, $field_name);

    // Assert default values for target and custom values.
    $this->drupalLogin($this->users['admin']);
    $this->drupalGet('user-anonymize-timestamp-field');
    foreach ($user_values as $key => $value) {
      $assert->elementTextContains('css', $key, $value);
    }

    // Assert anonymized values.
    $this->drupalLogin($this->users['target']);
    $this->drupalGet('user-anonymize-timestamp-field');
    foreach ($anonymized_values as $key => $value) {
      $assert->elementTextContains('css', $key, $value);
    }
  }

}
