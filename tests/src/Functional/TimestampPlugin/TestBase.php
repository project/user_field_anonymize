<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Functional\TimestampPlugin;

use Drupal\Tests\user_field_anonymize\Functional\FieldAnonymizeTestBase;

/**
 * Field anonymize timestamp test base.
 *
 * @group user_field_anonymize
 */
abstract class TestBase extends FieldAnonymizeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'field_ui',
    'datetime',
    'datetime_range',
    'user',
    'user_field_anonymize',
    'user_field_anonymize_test_views',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $pluginTestName = 'TimestampPlugin';

  /**
   * {@inheritdoc}
   */
  protected static $scenariosList = [
    'timestamp',
  ];

}
