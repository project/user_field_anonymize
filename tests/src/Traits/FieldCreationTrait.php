<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Traits;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Trait for field configuration related operations.
 */
trait FieldCreationTrait {

  /**
   * Creates a new field.
   *
   * @param string $field_name
   *   The name of the new field.
   * @param array $config
   *   The configuration to create the field with.
   */
  protected function createField(string $field_name, array $config): void {
    FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'user',
      'type' => $config['field_type'],
      'settings' => $config['storage_settings'],
      'cardinality' => $config['cardinality'] ?? 1,
    ])->save();

    FieldConfig::create([
      'field_name' => $field_name,
      'label' => $field_name,
      'entity_type' => 'user',
      'bundle' => 'user',
      'settings' => $config['instance_settings'],
      'third_party_settings' => $config['third_party_settings'] ?? [],
      'default_value' => $config['default_value_input'] ?? [],
    ])->save();
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository
      ->getViewDisplay('user', 'user')
      ->setComponent($field_name, ['type' => $config['field_view_type']])
      ->save();
    $display_repository->getFormDisplay('user', $config['form_entity_display'])
      ->setComponent($field_name, [
        'type' => $config['field_form_type'],
      ])
      ->save();
  }

  /**
   * Remove a field by name.
   *
   * @param string $field_name
   *   The field name.
   */
  protected function removeField(string $field_name): void {
    $field_config_storage = \Drupal::entityTypeManager()->getStorage('field_config');
    $field = current($field_config_storage->loadByProperties([
      'entity_type' => 'user',
      'field_name' => $field_name,
      'bundle' => 'user',
      'include_deleted' => TRUE,
    ]));
    if ($field) {
      $field->delete();
    }

    $field_storage_config_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config');
    $field_storage = current($field_storage_config_storage->loadByProperties([
      'field_name' => $field_name,
      'include_deleted' => TRUE,
    ]));
    if ($field_storage) {
      $field_storage->delete();
    }
  }

  /**
   * Update the field anonymized value.
   *
   * @param string $field_name
   *   The Field name to be updated.
   * @param array $value
   *   User field anonymize value.
   */
  protected function updateAnonymizeValue(string $field_name, array $value): void {
    $field_storage = \Drupal::entityTypeManager()
      ->getStorage('field_config')->load("user.user.{$field_name}");
    $field_storage->set('third_party_settings', [
      'user_field_anonymize' => [
        'enabled' => TRUE,
        'value' => $value,
      ],
    ]);
    $field_storage->save();
  }

}
