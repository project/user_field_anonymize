<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Traits;

use Drupal\Component\Serialization\Yaml;

/**
 * Trait that aids in test data providing.
 */
trait TestDataTrait {

  /**
   * Plugin name folder to fetch the fixtures.
   *
   * @var string
   */
  protected static $pluginTestName = '';

  /**
   * List of scenarios to be tested.
   *
   * @var string[]
   */
  protected static $scenariosList = [];

  /**
   * Data provider for field type tests.
   *
   * @return array
   *   The datasets.
   */
  public static function fieldAnonymizeScenarios(): array {
    $path = __DIR__ . '/../Functional/' . static::$pluginTestName . '/fixtures';
    $scenarios = [];
    foreach (static::$scenariosList as $scenario) {
      $field_types = Yaml::decode(file_get_contents("{$path}/{$scenario}.yml"));
      foreach ($field_types as $key => $field_type) {
        // Ensure unique test case key as the two files might share same keys.
        $suffix = 0;
        do {
          $candidate_key = $key . ($suffix ? sprintf(' (%s)', $suffix) : '');
          $suffix++;
        } while (isset($scenarios[$candidate_key]));
        $key = $candidate_key;
        $scenarios[$key] = $field_type;
      }
    }
    return $scenarios;
  }

}
