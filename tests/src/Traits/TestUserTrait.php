<?php

declare(strict_types=1);

namespace Drupal\Tests\user_field_anonymize\Traits;

use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Trait for setting up user entities.
 */
trait TestUserTrait {

  use UserCreationTrait;

  /**
   * Holds user entities.
   *
   * @var array
   */
  protected $users = [];

  /**
   * Creates user roles.
   */
  protected function createRoles(): void {
    Role::create([
      'id' => 'editor',
      'label' => 'editor',
      'permissions' => [
        'access user profiles',
        'set user profile anonymity',
      ],
    ])->save();
    Role::create([
      'id' => 'administrator',
      'label' => 'administrator',
    ])
      ->setIsAdmin(TRUE)
      ->save();
  }

  /**
   * Populates the users internal property.
   */
  protected function createUsers(): void {
    $this->users['target'] = $this->createUser(['access user profiles'], 'target');
    $this->users['target']->set('allowed_options', ['administrator' => 'administrator']);
    $this->users['target']->save();

    $this->users['admin'] = $this->createUser([], 'new_admin');
    $this->users['admin']->addRole('administrator');
    $this->users['admin']->set('allowed_options', ['administrator' => 'administrator']);
    $this->users['admin']->save();

    $this->users['observer'] = $this->createUser([], 'observer');
    $this->users['observer']->addRole('editor');
    $this->users['observer']->set('allowed_options', ['administrator' => 'administrator']);
    $this->users['observer']->save();
  }

  /**
   * Delete the stored user entities.
   */
  protected function deleteUsers(): void {
    foreach ($this->users as $user) {
      if (!$user instanceof UserInterface) {
        continue;
      }
      $user->delete();
    }
  }

  /**
   * Sets values for a given field for item in users.
   *
   * @param array $users
   *   Values grouped by username.
   * @param string $field_name
   *   Field name.
   */
  protected function setUserValues(array $users, string $field_name): void {
    foreach ($users as $username => $values) {
      $user = User::load($this->users[$username]->id());
      $user->get($field_name)
        ->setValue($values);
      $user->save();
    }
  }

}
