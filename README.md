# User Field Anonymize

Allows certain users to anonymize the values of user account fields
by replacing them with values set in the anonymization settings for
each individual field.

## Usage

 The fields could be mapped with plugin,
 for that purpose just follow the next instructions:

- Go to admin/config/people/user_field_anonymize.
- Set up the desired allowed roles.
- Map the field types with the desired plugins
(if there are any plugins available).
- Click on "Save configuration".

It will save the mapping which will be used to get the corresponding plugin id.
