<?php

/**
 * @file
 * The file to run post_update_N hooks.
 */

declare(strict_types=1);

/**
 * Set default value for 'allowed_options'.
 */
function user_field_anonymize_post_update_set_default_allowed_options(&$sandbox) {
  $database = \Drupal::database();
  $allowed_options = serialize([]);

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;

    // Count the number of users to update.
    $sandbox['max'] = $database->select('users_field_data')
      ->condition('allowed_options', NULL, 'IS')
      ->countQuery()
      ->execute()
      ->fetchField();

  }

  // Get a batch of users to update.
  $users = $database->select('users_field_data')
    ->fields('users_field_data', ['uid'])
    ->condition('allowed_options', NULL, 'IS')
    ->range(0, 50)
    ->execute()
    ->fetchCol();

  // Update each user.
  foreach ($users as $uid) {
    $database->update('users_field_data')
      ->fields(['allowed_options' => $allowed_options])
      ->condition('uid', $uid)
      ->execute();

    $sandbox['progress']++;
  }

  // Inform the batch API about our progress.
  $sandbox['#finished'] = empty($sandbox['max']) ? 1 : ($sandbox['progress'] / $sandbox['max']);

  // If not finished, provide a message.
  if ($sandbox['#finished'] < 1) {
    return t('Progress: @progress of @max', [
      '@progress' => $sandbox['progress'],
      '@max' => $sandbox['max'],
    ]);
  }
}
