<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user_field_anonymize\Traits\AvailableRolesTrait;
use Drupal\user_field_anonymize\UserFieldAnonymizePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure anonymization.
 */
class UserFieldAnonymizeSettingsForm extends ConfigFormBase {

  use AvailableRolesTrait;

  /**
   * Config settings name.
   */
  public const CONFIG_NAME = 'user_field_anonymize.settings';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The user field anonymize manager.
   *
   * @var \Drupal\user_field_anonymize\UserFieldAnonymizePluginManager
   */
  protected $userFieldAnonymize;

  /**
   * CompositeReferenceFieldManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager.
   * @param \Drupal\user_field_anonymize\UserFieldAnonymizePluginManager $user_field_anonymize
   *   The entity user field anonymize manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FieldTypePluginManagerInterface $field_type_manager, UserFieldAnonymizePluginManager $user_field_anonymize) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->userFieldAnonymize = $user_field_anonymize;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('plugin.manager.user_field_anonymize'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'user_field_anonymize_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_NAME);
    $form['restrict_to_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Restrict to roles'),
      '#description' => $this->t('Allow users to select the roles they would like to grant access to their data.'),
      '#options' => $this->buildOptions(),
      '#default_value' => $this->getRestrictedToRolesValues($config),
    ];
    $form['field_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Field types'),
      '#description' => $this->t('Match field types with plugin types.'),
      '#group' => 'advanced',
      '#open' => TRUE,
    ];
    $plugin_names = $this->getPluginNames();
    $fields_definitions = $this->fieldTypeManager->getUiDefinitions();

    foreach ($fields_definitions as $field) {
      if (in_array($field['id'], $this->userFieldAnonymize->getExcludedFieldTypes())) {
        continue;
      }
      $default_value = $config->get('field_options')[$field['id']]['plugin_id'];
      $field_id = $field['id'];
      if (isset($form["field_tab"]["field_options"])
        && in_array($field['id'], array_keys($form["field_tab"]["field_options"]))) {
        $field_id = $field['id'] . strval(random_int(0, mt_getrandmax()));
      }
      $form['field_tab']['field_options'][$field_id] = [
        '#type' => 'select',
        '#multiple' => FALSE,
        '#options' => $plugin_names,
        '#title' => $field['label']->getUntranslatedString(),
        '#default_value' => $default_value ?: 'Default',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config(self::CONFIG_NAME);
    $restricted_roles = $form_state->getValue('restrict_to_roles');
    // Handle the special options.
    $config->set('enable_admin_users', FALSE);
    $config->set('enable_all_users', FALSE);

    if (!empty($restricted_roles[self::getOnlyAdminId()])) {
      $config->set('enable_admin_users', TRUE);
    }
    if (!empty($restricted_roles[self::getAnyUserId()])) {
      $config->set('enable_all_users', TRUE);
    }

    unset($restricted_roles[self::getOnlyAdminId()]);
    unset($restricted_roles[self::getAnyUserId()]);
    $config->set('restrict_to_roles', array_keys(array_filter($restricted_roles)));
    $values = $form_state->getValues();
    $field_types = array_keys($this->fieldTypeManager->getUiDefinitions());

    foreach ($values as $field_type => $plugin_id) {
      if (in_array($field_type, $field_types)) {
        $field[$field_type] = [
          'field_type' => $field_type,
          'plugin_id' => $plugin_id,
        ];
        $config->set('field_options', $field);
      }
    }
    $config->save();
  }

  /**
   * Fetches the plugin names.
   *
   *   Returns the plugin names.
   */
  public function getPluginNames(): array {
    $plugin_names = [];
    $definitions = $this->userFieldAnonymize->getDefinitions();

    foreach ($definitions as $plugin_type) {
      $plugin_name = str_replace($plugin_type['provider'] . '_', '', (string) $plugin_type['id']);
      $plugin_names[$plugin_type['id']] = $plugin_name;
    }
    return $plugin_names;
  }

}
