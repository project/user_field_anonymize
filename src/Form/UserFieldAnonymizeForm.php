<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\user_field_anonymize\Traits\AvailableRolesTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure which roles are allowed to view user data.
 */
class UserFieldAnonymizeForm extends ContentEntityForm {

  use AvailableRolesTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId(): string {
    // Override the default form base ID just to get rid of the Contact module
    // addition by bypassing contact_form_user_form_alter().
    // @see contact_form_user_form_alter()
    return 'user_field_anonymize_form';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    // Remove standard user form fields.
    $form = array_diff_key($form, array_flip(Element::children($form)));

    $config = $this->config('user_field_anonymize.settings');
    $restrict_to_roles = $this->getRestrictedToRolesValues($config);

    if (!$restrict_to_roles) {
      $form['no_roles'] = [
        '#type' => 'inline_template',
        '#template' => '<p>{{ message }}</p>',
        '#context' => [
          'message' => $this->t('No roles have been set by the site administrator.'),
        ],
      ];
      return $form;
    }

    $field = $this->getEntity()->get('allowed_options');
    $roles_selected = !$field->isEmpty() ? $field->first()->getValue() : [
      'authenticated' => 'authenticated',
    ];
    $form['allowed_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Who can consult my data'),
      '#description' => $this->t('Select the type of users who are allowed to access your data.'),
      '#options' => $this->buildOptions($restrict_to_roles),
      '#default_value' => !$field->isEmpty() ? $field->first()->getValue() : $roles_selected,
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $value = parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('The changes have been saved.'));
    return $value;
  }

}
