<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an UserFieldAnonymize plugin manager.
 */
class UserFieldAnonymizePluginManager extends DefaultPluginManager {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Field types without default widget or, where anonymization makes no sense.
   *
   * @var string[]
   */
  protected static $excludeFieldTypes = [
    'file',
    'comment',
    'boolean',
  ];

  /**
   * Constructs a UserFieldAnonymizePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/UserFieldAnonymize',
      $namespaces,
      $module_handler,
      'Drupal\user_field_anonymize\Plugin\UserFieldAnonymizePluginInterface',
      'Drupal\user_field_anonymize\Annotation\UserFieldAnonymize',
    );
    $this->alterInfo('user_field_anonymize_info');
    $this->setCacheBackend($cache_backend, 'user_field_anonymize_plugins');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass($plugin_id, $plugin_definition);

    if (is_subclass_of($plugin_class, 'Drupal\Core\Plugin\ContainerFactoryPluginInterface')) {
      // @phpstan-ignore-next-line
      return $plugin_class::create(\Drupal::getContainer(), $configuration, $plugin_id, $plugin_definition);
    }

    return new $plugin_class($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    $plugin_id = $this->getPluginForFieldType($options['field_type']);
    return $this->createInstance($plugin_id);
  }

  /**
   * Gets the excluded field types.
   *
   * @return string[]
   *   The field types list.
   */
  public function getExcludedFieldTypes(): array {
    return self::$excludeFieldTypes;
  }

  /**
   * Checks if there is a plugin mapped for the field type.
   *
   * @param string $field_type
   *   The field type.
   *
   * @return bool
   *   If there is a plugin available.
   */
  public function hasPluginForFieldType(string $field_type): bool {
    if (in_array($field_type, self::$excludeFieldTypes)) {
      return FALSE;
    }

    $field_options = $this->configFactory
      ->get('user_field_anonymize.settings')
      ->get('field_options');

    if (!isset($field_options[$field_type])) {
      return FALSE;
    }

    $field = $field_options[$field_type];

    if (empty($field['plugin_id'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Gets the plugin id from the field type.
   *
   * @param string $field_type
   *   Field type.
   *
   * @return string
   *   Returns plugin id.
   */
  protected function getPluginForFieldType(string $field_type): string {
    if (!$this->hasPluginForFieldType($field_type)) {
      return 'user_field_anonymize_default';
    }

    $field_options = $this->configFactory
      ->get('user_field_anonymize.settings')
      ->get('field_options');

    return $field_options[$field_type]['plugin_id'];
  }

}
