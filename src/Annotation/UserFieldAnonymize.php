<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a UserFieldAnonymize annotation object.
 *
 * @Annotation
 */
class UserFieldAnonymize extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
