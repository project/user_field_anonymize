<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Traits;

use Drupal\Core\Config\Config;

/**
 * Available roles trait.
 */
trait AvailableRolesTrait {

  /**
   * Gets the Only Admin id.
   *
   * @return string
   *   The option id.
   */
  public static function getOnlyAdminId(): string {
    return 'only admin';
  }

  /**
   * Gets the Any User id.
   *
   * @return string
   *   The option id.
   */
  public static function getAnyUserId(): string {
    return 'any user';
  }

  /**
   * Return available roles.
   *
   * @param array|null $config
   *   Available roles configured by the site administrator.
   *
   * @return array
   *   Filtered roles.
   */
  public function buildOptions(?array $config = NULL): array {
    $options = [];

    $system_roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    // Additional options.
    if (!$config || in_array(self::getOnlyAdminId(), $config)) {
      $options[self::getOnlyAdminId()] = (string) $this->t('Only site Administrators');
    }
    if (!$config || in_array(self::getAnyUserId(), $config)) {
      $options[self::getAnyUserId()] = (string) $this->t('Any user');
    }

    foreach ($system_roles as $key => $role) {
      if (in_array($key, ['anonymous']) || ($config && !in_array($key, $config))) {
        continue;
      }
      $options[$key] = $role->label();
    }

    return $options;
  }

  /**
   * Gets the joined settings as roles.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The module settings configuration.
   *
   * @return array
   *   The joined settings.
   */
  public function getRestrictedToRolesValues(Config $config): array {
    $roles = $config->get('restrict_to_roles') ?: [];

    if ($config->get('enable_admin_users')) {
      $roles[] = self::getOnlyAdminId();
    }

    if ($config->get('enable_all_users')) {
      $roles[] = self::getAnyUserId();
    }

    return $roles;
  }

}
