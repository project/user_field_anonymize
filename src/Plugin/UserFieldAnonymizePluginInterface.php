<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for UserFieldAnonymizePlugin.
 */
interface UserFieldAnonymizePluginInterface {

  /**
   * Builds the anonymize subform.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildAnonymizeSubForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Validates the anonymize form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateAnonymizeSubForm(array $form, FormStateInterface &$form_state): void;

  /**
   * Saves the anonymize values.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitAnonymizeSubForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Alters the build to anonymize values.
   *
   * @param array $values
   *   The anonymize build.
   * @param object $items
   *   The items.
   */
  public function getAnonymizeBuild(array $values, &$items): void;

}
