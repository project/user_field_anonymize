<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Plugin;

use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for UserFieldAnonymize plugins.
 */
abstract class UserFieldAnonymizePluginBase extends PluginBase implements UserFieldAnonymizePluginInterface, ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * Entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * UserFieldAnonymizePluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager service.
   * @param \Drupal\Core\Entity\EntityRepository $entity_repository
   *   Entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FieldTypePluginManagerInterface $field_type_manager, EntityRepository $entity_repository, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->fieldTypeManager = $field_type_manager;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildAnonymizeSubForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\field\Entity\FieldConfig $entity */
    $entity = $form_state->getFormObject()->getEntity();
    $field_name = $entity->getName();
    /** @var \Drupal\Core\Field\FieldItemList $items */
    $items = $form['#entity']->get($field_name);
    $values = $entity->getThirdPartySetting('user_field_anonymize', 'value');
    $processed_values = $items::processDefaultValue($values, $items->getEntity(), $items->getFieldDefinition());
    $items->setValue($processed_values);
    $element = $items->defaultValuesForm($form, $form_state);

    $element_enabled_state = ':input[name="third_party_settings[user_field_anonymize][enabled]"]';
    $element['widget']['#parents'] = [
      'third_party_settings',
      'user_field_anonymize',
      'value',
      $field_name,
    ];

    $element['#states'] = [
      'visible' => [
        $element_enabled_state => ['checked' => TRUE],
      ],
      'required' => [
        $element_enabled_state => ['checked' => TRUE],
      ],
    ];
    $form['third_party_settings']['user_field_anonymize']['value'] = $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateAnonymizeSubForm(array $form, FormStateInterface &$form_state): void {
    $entity = $form_state->getFormObject()->getEntity();
    $items = $form['#entity']->get($entity->getName());
    $element = $items->defaultValuesForm($form, $form_state);
    $items->defaultValuesFormValidate($element, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitAnonymizeSubForm(array &$form, FormStateInterface $form_state): void {
    $element = $form['third_party_settings']['user_field_anonymize']['value'];

    if (!$element) {
      return;
    }

    $entity = $form_state->getFormObject()->getEntity();
    $field_name = $entity->getName();
    /** @var \Drupal\Core\Field\FieldItemList $items */
    $items = $form['#entity']->get($field_name);

    // We construct #parents so FieldItemList::defaultValuesFormSubmit() finds
    // our values and not the default_value_input.
    // @see \Drupal\Core\Field\WidgetBase::extractFormValues().
    $element['#parents'] = [
      'third_party_settings',
      'user_field_anonymize',
      'value',
    ];
    // Field information is stored in $form_state during defaultValuesForm().
    // When WidgetBase::extractFormValues() is called during
    // defaultValuesFormSubmit delta mapping is put in $form_state,
    // so that flagErrors() can use it.
    // If the $field_state is not set in storage for our anonymize widget then
    // WidgetBase::setWidgetState() will throw an error when passing empty
    // values.
    // This doesn't happen when we have values because the $items loop before
    // populates $field_state.
    // @see \Drupal\Core\Field\WidgetBase::extractFormValues().
    if (!WidgetBase::getWidgetState($element['#parents'], $field_name, $form_state)) {
      $field_state = [
        'items_count' => count($items),
        'array_parents' => [],
      ];
      WidgetBase::setWidgetState($element['#parents'], $field_name, $form_state, $field_state);
    }

    $value = $items->defaultValuesFormSubmit($element, $form, $form_state);
    $form_state->setValue([
      'third_party_settings',
      'user_field_anonymize',
      'value',
    ], $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getAnonymizeBuild(array $values, &$items): void {
    if (empty($values)) {
      return;
    }
    $processed_values = $items::processDefaultValue($values, $items->getEntity(), $items->getFieldDefinition());
    $items->setValue($processed_values);
  }

}
