<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Plugin\UserFieldAnonymize;

use Drupal\user_field_anonymize\Plugin\UserFieldAnonymizePluginBase;

/**
 * Default plugin for UserFieldAnonymize.
 *
 * @UserFieldAnonymize(
 *   id = "user_field_anonymize_default",
 *   label = @Translation("User Field Anonymize default plugin")
 * )
 */
class DefaultPlugin extends UserFieldAnonymizePluginBase {

}
