<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Plugin\UserFieldAnonymize;

use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\user_field_anonymize\Plugin\UserFieldAnonymizePluginBase;

/**
 * Defines UserFieldAnonymize image plugin.
 *
 * @UserFieldAnonymize(
 *   id = "user_field_anonymize_image",
 *   label = @Translation("User Field Anonymize image plugin")
 * )
 */
class ImagePlugin extends UserFieldAnonymizePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildAnonymizeSubForm(array &$form, FormStateInterface $form_state): void {
    $element = [];
    /** @var \Drupal\field\Entity\FieldConfig $entity */
    $entity = $form_state->getFormObject()->getEntity();
    $field_name = $entity->getName();
    /** @var \Drupal\file\Plugin\Field\FieldType\FileFieldItemList $items */
    $items = $form['#entity']->get($field_name);
    $values = $entity->getThirdPartySetting('user_field_anonymize', 'value');
    $settings = $items->getSettings();

    // FileFieldItemList::defaultValuesForm() is an empty implementation.
    // We mimic ImageItem::defaultImageForm() behaviour because it is a
    // protected method.
    // @see \Drupal\image\Plugin\Field\FieldType\ImageItem::defaultImageForm().
    $fids = [];
    $uuid = $values['uuid'];
    if ($uuid && !isset($uuid['fids'])
      && $file = $this->entityRepository->loadEntityByUuid('file', $uuid)) {
      $fids[0] = $file->id();
    }

    $element['uuid'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#description' => $this->t('Image to be shown if no image is uploaded.'),
      '#default_value' => $fids,
      '#upload_location' => $settings['uri_scheme'] . '://default_images/',
      '#element_validate' => [
        '\Drupal\file\Element\ManagedFile::validateManagedFile',
        [ImageItem::class, 'validateDefaultImageForm'],
      ],
    ];
    $element['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternative text'),
      '#description' => $this->t('Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.'),
      '#default_value' => $values['alt'],
      '#maxlength' => 512,
    ];
    $element['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('The title attribute is used as a tooltip when the mouse hovers over the image.'),
      '#default_value' => $values['title'],
      '#maxlength' => 1024,
    ];
    $element['width'] = [
      '#type' => 'value',
      '#value' => $values['width'],
    ];
    $element['height'] = [
      '#type' => 'value',
      '#value' => $values['height'],
    ];
    $form['third_party_settings']['user_field_anonymize']['value'] = $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitAnonymizeSubForm(array &$form, FormStateInterface $form_state): void {
    // Since FileFieldItemList has an empty defaultValueForm implementation,
    // $form['default_value'] is never set, so defaultValuesFormSubmit is never
    // called for the default value, thus neither should we.
    // @see \Drupal\file\Plugin\Field\FieldType\FileFieldItemList::defaultValuesForm()
    // @see \Drupal\field_ui\Form\FieldConfigEditForm::submitForm()
  }

  /**
   * {@inheritdoc}
   */
  public function validateAnonymizeSubForm(array $form, FormStateInterface &$form_state): void {
    // Validation is handled by uuid][#element_validate.
    // @see buildAnonymizeSubForm().
  }

  /**
   * {@inheritdoc}
   */
  public function getAnonymizeBuild(array $values, &$items): void {
    if (!isset($values['uuid'])) {
      return;
    }

    $file = $this->loadFileEntityByUuid($values['uuid']);
    if (empty($file)) {
      return;
    }
    $values['target_id'] = $file->id();
    $items->setValue([$values]);
  }

  /**
   * Loads a file entity by UUID.
   *
   * @param string $uuid
   *   The UUID to load the entity by.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false|null
   *   The file entity or FALSE.
   */
  protected function loadFileEntityByUuid(string $uuid) {
    if (empty($uuid)) {
      return FALSE;
    }

    return $this->entityRepository->loadEntityByUuid('file', $uuid);
  }

}
