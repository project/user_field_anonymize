<?php

declare(strict_types=1);

namespace Drupal\user_field_anonymize\Plugin\UserFieldAnonymize;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList;
use Drupal\user_field_anonymize\Plugin\UserFieldAnonymizePluginBase;

/**
 * Defines UserFieldAnonymize date plugin.
 *
 * @UserFieldAnonymize(
 *   id = "user_field_anonymize_date",
 *   label = @Translation("User Field Anonymize date plugin")
 * )
 */
class DatetimePlugin extends UserFieldAnonymizePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildAnonymizeSubForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\field\Entity\FieldConfig $entity */
    $entity = $form_state->getFormObject()->getEntity();
    $field_name = $entity->getName();
    /** @var \Drupal\Core\Field\FieldItemList $items */
    $items = $form['#entity']->get($field_name);
    $values = $entity->getThirdPartySetting('user_field_anonymize', 'value')[0];
    $processed_values = $items::processDefaultValue($values, $items->getEntity(), $items->getFieldDefinition());
    $items->setValue($processed_values);
    $element = $items->defaultValuesForm($form, $form_state);

    // Date field items fetch the values directly from the field definition,
    // so we have to overwrite them.
    $element['default_date_type']['#default_value'] = $processed_values['default_date_type'];
    $element['default_date']['#default_value'] = $processed_values['default_date'];

    $element_enabled_state = ':input[name="third_party_settings[user_field_anonymize][enabled]"]';
    $element['#parents'] = [
      'third_party_settings',
      'user_field_anonymize',
      'value',
    ];
    $element['default_date_type']['#states'] = [
      'visible' => [
        $element_enabled_state => ['checked' => TRUE],
      ],
      'required' => [
        $element_enabled_state => ['checked' => TRUE],
      ],
    ];
    $element_default_date_state = ':input[name="third_party_settings[user_field_anonymize][value][default_date_type]"]';
    $element['default_date']['#states'] = [
      'visible' => [
        $element_enabled_state => ['checked' => TRUE],
        'and' => 'and',
        $element_default_date_state  => ['value' => DateTimeFieldItemList::DEFAULT_VALUE_CUSTOM],
      ],
      'required' => [
        $element_enabled_state => ['checked' => TRUE],
        'and' => 'and',
        $element_default_date_state  => ['value' => DateTimeFieldItemList::DEFAULT_VALUE_CUSTOM],
      ],
    ];

    // Modify widget for Date range item.
    if ($items instanceof DateRangeFieldItemList) {
      $element['default_end_date_type']['#default_value'] = $processed_values['default_end_date_type'];
      $element['default_end_date']['#default_value'] = $processed_values['default_end_date'];

      $element_default_end_date_state = ':input[name="third_party_settings[user_field_anonymize][value][default_end_date_type]"]';
      $element['default_end_date_type']['#states'] = [
        'visible' => [
          $element_enabled_state => ['checked' => TRUE],
        ],
      ];
      $element['default_end_date']['#states'] = [
        'visible' => [
          $element_enabled_state => ['checked' => TRUE],
          'and' => 'and',
          $element_default_end_date_state => ['value' => DateTimeFieldItemList::DEFAULT_VALUE_CUSTOM],
        ],
      ];
    }

    $form['third_party_settings']['user_field_anonymize']['value'] = $element;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList::defaultValuesFormValidate()
   * @see \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList::defaultValuesFormValidate()
   */
  public function validateAnonymizeSubForm(array $form, FormStateInterface &$form_state): void {
    $element_name = 'third_party_settings][user_field_anonymize][value][';
    $element_path = ['third_party_settings', 'user_field_anonymize', 'value'];
    // We can't call defaultValuesFormValidate since it gets its values directly
    // from the $form state default_value_input, so we need to mimic it.
    if ($form_state->getValue(array_merge($element_path, ['default_date_type'])) === DateTimeFieldItemList::DEFAULT_VALUE_CUSTOM) {
      $is_strtotime = @strtotime((string) $form_state->getValue(array_merge($element_path, ['default_date'])));
      if (!$is_strtotime) {
        $form_state->setErrorByName(
          $element_name . 'default_date',
          $this->t('The relative date value for anonymization entered is invalid.')
        );
      }
    }
    if ($form_state->getValue(array_merge($element_path, ['default_end_date_type'])) === DateTimeFieldItemList::DEFAULT_VALUE_CUSTOM) {
      $is_strtotime = @strtotime((string) $form_state->getValue(array_merge($element_path, ['default_end_date'])));
      if (!$is_strtotime) {
        $form_state->setErrorByName(
          $element_name . 'default_end_date',
          $this->t('The relative end date value for anonymization entered is invalid.')
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList::defaultValuesFormSubmit()
   * @see \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList::defaultValuesFormSubmit()
   */
  public function submitAnonymizeSubForm(array &$form, FormStateInterface $form_state): void {
    $path = ['third_party_settings', 'user_field_anonymize', 'value'];
    $key_date_type = array_merge($path, ['default_date_type']);
    $key_end_date_type = array_merge($path, ['default_end_date_type']);
    $value = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    // We can't call defaultValuesFormSubmit since it gets its values directly
    // from the $form state default_value_input, so we need to mimic it.
    if ($form_state->getValue($key_date_type) || $form_state->getValue($key_end_date_type)) {
      if ($form_state->getValue($key_date_type) == DateTimeFieldItemList::DEFAULT_VALUE_NOW) {
        $value['default_date'] = DateTimeFieldItemList::DEFAULT_VALUE_NOW;
      }
      if ($form_state->getValue($key_end_date_type) == DateTimeFieldItemList::DEFAULT_VALUE_NOW) {
        $value['default_end_date'] = DateTimeFieldItemList::DEFAULT_VALUE_NOW;
      }
      $form_state->setValue($path, [$value]);
      return;
    }
    $form_state->setValue($path, []);
  }

  /**
   * {@inheritdoc}
   */
  public function getAnonymizeBuild(array $values, &$items): void {
    $date_values = DateTimeFieldItemList::processDefaultValue($values, $items->getEntity(), $items->getFieldDefinition());

    if ($items instanceof DateRangeFieldItemList) {
      $date_values = DateRangeFieldItemList::processDefaultValue($values, $items->getEntity(), $items->getFieldDefinition());
    }
    $items->setValue($date_values);
  }

}
